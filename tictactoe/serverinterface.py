from states import StateType
from messages import MessageType
from recvmanager import Recvmanager
from sendmanager import Sendmanager
from gamemanager import TicTacToe


__author__ = 'ian'


class ServerInterface:
    def __init__(self, server, gamemanager):
        self.state = StateType.Idle
        self.server = server
        self.sendmanager = Sendmanager(server)
        self.recvmanager = Recvmanager(self, self.sendmanager)
        self.gamemanager = gamemanager

    def startGame(self):
        self.gamemanager.startGame()
        self.sendmanager.create_msg(MessageType.StartGame)
        self.recvmanager.proc_msg(self.server.receive_msg())
        self.state = StateType.WaitForTurn
        self._gameLoop()

    def displayTie(self):
        print("It's a Tie!")

    def displayLost(self):
        print("You failed!")

    def displayWin(self):
        print("You won!")

    def _gameLoop(self):
        while self.gamemanager.state != StateType.EndGame:
            if self.state == StateType.WaitForTurn:
                self.sendmanager.create_msg(MessageType.Info, self.gamemanager.sendInfo())
                self.recvmanager.proc_msg(self.server.receive_msg())

                self.sendmanager.create_msg(self.gamemanager.getTurnmsg())
                print("After request")
                self.recvmanager.proc_msg(self.server.receive_msg())
                print("After answer")
                if isinstance(self.gamemanager, TicTacToe):
                    self.state = StateType.Turn
            elif self.state == StateType.Turn:
                self.gamemanager.getTurn()
                self.state = StateType.WaitForTurn
            elif self.gamemanager.state == StateType.EndGame:
                self.sendmanager.create_msg(MessageType.EndGame)

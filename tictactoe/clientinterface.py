from states import StateType
from recvmanager import Recvmanager
from sendmanager import Sendmanager


__author__ = 'ian'


class ClientInterface:
    def __init__(self, client):
        self.state = StateType.Idle
        self.client = client
        self.sendmanager = Sendmanager(client)
        self.recvmanager = Recvmanager(self, self.sendmanager)

    def startGame(self):
        self.state = StateType.WaitForTurn
        self._gameLoop()

    def displayTie(self):
        print("It's a Tie!")

    def displayLost(self):
        print("You failed!")

    def displayWin(self):
        print("You won!")

    def listenForStart(self):
        while self.state != StateType.StartGame:
            self.recvmanager.proc_msg(self.client.receive_msg())

    def _gameLoop(self):
        while self.state != StateType.EndGame:
            msg = self.client.receive_msg()
            self.recvmanager.proc_msg(msg)

            if self.state == StateType.Tie:
                self.displayTie()
                self.state = StateType.EndGame
            elif self.state == StateType.Loss:
                self.displayLost()
                self.state = StateType.EndGame
            elif self.state == StateType.Win:
                self.displayWin()
                self.state = StateType.EndGame



from unittest import TestCase
from tictactoe.map import Map

__author__ = 'ian'


class TestMap(TestCase):
    def test_check_full_line_should_end_game(self):
        player = 'o'
        gamemap = Map()
        gamemap.update(0, player)
        gamemap.update(1, player)
        gamemap.update(2, player)
        self.assertEqual(gamemap.is_end(), True)

    def test_player_at_pos__should_return_correct_player(self):
        player = 'o'
        gamemap = Map()
        gamemap.update(0, player)
        self.assertEqual(gamemap.player_at_pos(0), 'o')

    def test_clear_map(self):
        player = 'o'
        gamemap = Map()
        gamemap.update(0, player)
        gamemap.clear()
        self.assertEqual(gamemap.player_at_pos(0), ' ')

    def test_copy_map(self):
        gamemap = Map()
        mapstr = "oxoxxxooo"
        gamemap.copy_map(mapstr)
        self.assertEqual(gamemap.mapgrid, ['o', 'x', 'o', 'x', 'x', 'x', 'o', 'o', 'o'])

    def test_valid_position_should_pass(self):
        gamemap = Map()
        gamemap.valid_position(4)



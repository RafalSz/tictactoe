from unittest import TestCase
from tictactoe.messages import MessageType
from tictactoe.clientinterface import ClientInterface
from tictactoe.recvmanager import Recvmanager
__author__ = 'ian'


class TestRecvmanager(TestCase):
    def test_proc_msg(self):
        msg = str(MessageType.MapState) + "oooxxxooo"
        data = msg[2:len(msg)]
        self.assertEqual("oooxxxooo", data)
    def test_proc_msg_mapstate_should_change_mapgrid(self):
        clientinterface = ClientInterface(None)
        recvmanager = Recvmanager(clientinterface)
        msg = str(MessageType.MapState) + "oooxxxooo"
        recvmanager.proc_msg(msg)
        self.assertEqual(clientinterface.map.mapgrid, list("oooxxxooo"))

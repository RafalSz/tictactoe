from unittest import TestCase
from tictactoe.gamemanager import TicTacToe

__author__ = 'ian'


class TestGamemanager(TestCase):
    def test_turn(self):
        gamemanager = TicTacToe()
        gamemanager.turn('o', 0)
        gamemanager.turn('o', 1)
        gamemanager.turn('o', 2)
        gamemanager.turn('o', 3)

        self.assertEqual(''.join(gamemanager.map.mapgrid), "oooo     ")

    def test_and(self):
        self.assertTrue(True and True)
        self.assertTrue('x' == 'x')

    def test_valid_player(self):
        gamemanager = TicTacToe()
        self.assertTrue(gamemanager.valid_player('o'))


from unittest import TestCase
from tictactoe.map import Map

__author__ = 'ian'


class TestServerInterface(TestCase):
    def test_sendMapInfo(self):
        self.map = Map()
        mapstr = "oooxxxooo"
        self.map.copy_map(mapstr)
        mapstate = ''.join(self.map.mapgrid)
        self.assertEqual(mapstate, mapstr)

from unittest import TestCase
from tictactoe.messages import StartGameMessage
from tictactoe.messages import EndGameMessage
from tictactoe.messages import MessageType

__author__ = 'ian'


class TestStartGameMessage(TestCase):
    def test_create_start_game_message_should_return_good_string(self):
        msg = StartGameMessage('')
        self.assertEqual(msg.create(), str(MessageType.StartGame))

    def test_create_end_game_message_should_return_good_string(self):
        msg = EndGameMessage('')
        self.assertEqual(msg.create(), str(MessageType.EndGame))






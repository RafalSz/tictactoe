from map import Map
from states import StateType
from messages import MessageType
import random

__author__ = 'ian'


class TicTacToe:
    def __init__(self, user=None):
        self.map = Map()
        self.user = user
        self.player1 = 'o'
        self.player2 = 'x'
        self.state = StateType.Idle

    def setPlayer1(self, player):
        self.player1 = player

    def startGame(self):
        self.state = StateType.StartGame

    def setPlayer2(self, player):
        self.player2 = player

    def valid_player(self, player):
        if player == self.player1 or player == self.player2:
            return True
        else:
            return False

    def sendInfo(self):
        return ''.join(self.map.mapgrid)

    def getTurnmsg(self):
        return MessageType.GivePosition

    def getTurn(self):
        position = input("Type the position 0-8: ")
        self.turn('o', position)

    def turn(self, player, position):
        if self.valid_player(player) and self.map.valid_position(position):
            self.map.update(position, player)
            if self.map.is_end():
                self.state = StateType.EndGame
            return True
        else:
            print("BadPosition")
            return False


class MoreLess:
    def __init__(self):
        self.number = 0
        self.maxnum = 100
        self.state = StateType.Idle
        self.msgstate = "Not started"

    def _castlots(self):
        self.number = random.randint(0, self.maxnum)

    def _setmax(self, number):
        self.maxnum = number

    def sendInfo(self):
        return self.msgstate

    def getTurnmsg(self):
        return MessageType.GiveNumber

    def getTurn(self):
        number = input("Type the number")
        self.turn(number)

    def startGame(self):
        self._castlots()
        self.state = StateType.StartGame

    def turn(self, number):
        if number > self.number:
            self.msgstate =  "Too big"
        if number < self.number:
            self.msgstate = "Too small"
        if number == self.number:
            self.state = StateType.EndGame
            self.msgstate = "You win!"
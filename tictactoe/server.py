import socket

__author__ = 'ian'


class Server:
    def __init__(self, address, port, data_size):
        self.data_size = data_size
        self._createTcpIpSocket()
        self._bindSocketToThePort(address, port)
        self.sock.listen(1)
        self.connection, client_address = self.sock.accept()

    def close_socket(self):
        self.connection.close()

    def send_msg(self, msg):
        self.connection.send(msg)

    def receive_msg(self):
        return self.connection.recv(self.data_size)

    def _createTcpIpSocket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def _bindSocketToThePort(self, address, port):
        server_address = (address, port)
        print('bind to %s port %s' % server_address)
        self.sock.bind(server_address)





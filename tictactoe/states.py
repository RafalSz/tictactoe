import enum

__author__ = 'ian'


class StateType(enum.Enum):
    Idle = 10
    StartGame = 11
    EndGame = 12
    Turn = 13
    Tie = 14
    Loss = 15
    Win = 16
    WaitForTurn = 17

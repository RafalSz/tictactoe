from messages import *

__author__ = 'ian'


class Sendmanager:
    def __init__(self, user):
        self.user = user

    def create_msg(self, msgtype, data=''):
        if msgtype == MessageType.StartGame:
            self.user.send_msg(StartGameMessage(data).create())
        elif msgtype == MessageType.EndGame:
            self.user.send_msg(EndGameMessage(data).create())
        elif msgtype == MessageType.Error:
            self.user.send_msg(ErrorMessage(data).create())
        elif msgtype == MessageType.Control:
            self.user.send_msg(ControlMessage(data).create())
        elif msgtype == MessageType.MapState:
            self.user.send_msg(MapStateMessage(data).create())
        elif msgtype == MessageType.Position:
            self.user.send_msg(PositionMessage(data).create())
        elif msgtype == MessageType.GiveNumber:
            self.user.send_msg(GiveNumberMessage(data).create())
        elif msgtype == MessageType.GivePosition:
            self.user.send_msg(GivePositionMessage(data).create())
        elif msgtype == MessageType.TicTacToe:
            self.user.send_msg(TicTacToeMessage(data).create())
        elif msgtype == MessageType.MoreLess:
            self.user.send_msg(MoreLessMessage(data).create())
        elif msgtype == MessageType.Info:
            self.user.send_msg(InfoMessage(data).create())
        elif msgtype == MessageType.Number:
            self.user.send_msg(NumberMessage(data).create())
        elif msgtype == MessageType.NoType:
            self.user.send_msg(data)

from messages import MessageType
from states import StateType
from map import Map

__author__ = 'ian'


class Recvmanager:
    def __init__(self, clientinterface, sendmanager):
        self.clientinterface = clientinterface
        self.sendmanager = sendmanager

    def proc_msg(self, msg):
        msgtype = int(msg[:2])
        data = msg[2:len(msg)]
        if msgtype == MessageType.MapState:
            map = Map()
            map.copy_map(data)
            map.display()
            position = input("Type the position 0-8: ")
            self.sendmanager.create_msg(MessageType.Position, position)
        elif msgtype == MessageType.StartGame:
            print(":::Game Start:::")
            self.clientinterface.state = StateType.StartGame
            self.sendmanager.create_msg(MessageType.Control)
        elif msgtype == MessageType.Position:
            data = int(data)
            self.clientinterface.gamemanager.turn('x', data)
        elif msgtype == MessageType.Number:
            data = int(data)
            self.clientinterface.gamemanager.turn(data)
        elif msgtype == MessageType.EndGame:
            self.clientinterface.state = StateType.EndGame
        elif msgtype == MessageType.GiveNumber:
            number = input("Type the number 0-100")
            print(number)
            self.sendmanager.create_msg(MessageType.Number, number)
        elif msgtype == MessageType.GivePosition:
            position = input("Type the position 0-8: ")
            self.sendmanager.create_msg(MessageType.Position, position)
        elif msgtype == MessageType.Info:
            print(data)
            self.sendmanager.create_msg(MessageType.Control)
        elif msgtype == MessageType.EndGame:
            self.clientinterface.state = StateType.EndGame
        elif msgtype == MessageType.Control:
            pass
        elif msgtype == MessageType.GameSelectionQuestion:
            game = input("Choose game: 1(TicTacToe) 2(MoreLess)")
            if game == 1:
                self.sendmanager.create_msg(MessageType.TicTacToe)
            else:
                self.sendmanager.create_msg(MessageType.MoreLess)

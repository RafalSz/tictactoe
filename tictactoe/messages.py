__author__ = 'ian'

import enum
from states import StateType

class MessageType(enum.Enum):
    NoType = ''
    Control = 20
    Info = 21
    StartGame = StateType.StartGame
    EndGame = StateType.EndGame
    MapState = 23
    Tie = StateType.Tie
    Win = StateType.Win
    Loss = StateType.Loss
    Position = 24
    Text = 25
    Error = 26
    GiveNumber = 27
    GivePosition = 28
    Number = 29
    GameSelection = 30
    GameSelectionQuestion = 31
    TicTacToe = 32
    MoreLess = 33

class Message:
    def __init__(self, data, option):
        self.option = option
        self.data = data

    def create(self):
        return str(self.option) + str(self.data)


class StartGameMessage(Message):
    def __init__(self, data):
        self.option = MessageType.StartGame
        Message.__init__(self, data, self.option)

class EndGameMessage(Message):
    def __init__(self, data):
        self.option = MessageType.EndGame
        Message.__init__(self, data, self.option)

class MapStateMessage(Message):
    def __init__(self, data):
        self.option = MessageType.MapState
        Message.__init__(self, data, self.option)

class TieMessage(Message):
    def __init__(self, data):
        self.option = MessageType.Tie
        Message.__init__(self, data, self.option)

class PositionMessage(Message):
    def __init__(self, data):
        self.option = MessageType.Position
        Message.__init__(self, data, self.option)

class ErrorMessage(Message):
    def __init__(self, data):
        self.option = MessageType.Error
        Message.__init__(self, data, self.option)

class ControlMessage(Message):
    def __init__(self, data):
        self.option = MessageType.Control
        Message.__init__(self, data, self.option)

class InfoMessage(Message):
    def __init__(self, data):
        self.option = MessageType.Info
        Message.__init__(self, data, self.option)

class NumberMessage(Message):
    def __init__(self, data):
        self.option = MessageType.Number
        Message.__init__(self, data, self.option)

class GiveNumberMessage(Message):
    def __init__(self, data):
        self.option = MessageType.GiveNumber
        Message.__init__(self, data, self.option)

class GivePositionMessage(Message):
    def __init__(self, data):
        self.option = MessageType.GivePosition
        Message.__init__(self, data, self.option)

class GameSelectionMessage(Message):
    def __init__(self, data):
        self.option = MessageType.GameSelection
        Message.__init__(self, data, self.option)

class GameSelectionQestionMessage(Message):
    def __init__(self, data):
        self.option = MessageType.GameSelectionQuestion
        Message.__init__(self, data, self.option)

class TicTacToeMessage(Message):
    def __init__(self, data):
        self.option = MessageType.TicTacToe
        Message.__init__(self, data, self.option)

class MoreLessMessage(Message):
    def __init__(self, data):
        self.option = MessageType.MoreLess
        Message.__init__(self, data, self.option)
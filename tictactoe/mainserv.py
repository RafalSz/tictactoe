__author__ = 'ian'

from server import Server
from serverinterface import ServerInterface
from messages import MessageType
from gamemanager import TicTacToe
from gamemanager import MoreLess


if __name__ == "__main__":
    host = 'localhost'
    port = 50001
    data_size = 1024
    server = Server(host, port, data_size)
    server.send_msg(str(MessageType.GameSelectionQuestion))
    if int(server.receive_msg()) == MessageType.TicTacToe:
        game = TicTacToe()
    else:
        game = MoreLess()

    serverinterface = ServerInterface(server, game)
    serverinterface.startGame()
    server.close_socket()

import socket

__author__ = 'ian'


class Client:
    def __init__(self, address, port, data_size):
        self.data_size = data_size
        self._create_tcp_ip_socket()
        self._connect_to_server(address, port)

    def send_msg(self, msg):
        self.sock.send(msg)

    def receive_msg(self):
        return self.sock.recv(self.data_size)

    def close_socket(self):
        self.sock.close()

    def _create_tcp_ip_socket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def _connect_to_server(self, address, port):
        server_address = (address, port)
        print('connecting to %s port %s' % server_address)
        self.sock.connect(server_address)


__author__ = 'ian'

MAX_POSITIONS = 9


class Map:
    def __init__(self):
        self.mapgrid = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
        self.check_set = [None] * 8
        self.filled = 0
        self.winner = ' '
        
    def display(self):
            print(self.mapgrid[0] + '|' + self.mapgrid[1] + '|' + self.mapgrid[2] + '\n' +
                  self.mapgrid[3] + '|' + self.mapgrid[4] + '|' + self.mapgrid[5] + '\n' +
                  self.mapgrid[6] + '|' + self.mapgrid[7] + '|' + self.mapgrid[8])

    def clear(self):
        self.mapgrid = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']

    def update(self, position, player):
        self.mapgrid[position] = player
        self.filled += 1

    def copy_map(self, map):
        self.mapgrid = list(map)

    def player_at_pos(self, position):
        return self.mapgrid[position]

    def valid_position(self, position):
        if position < 0 or position > 8:
            return False
        if self.mapgrid[position] != ' ':
            return False
        return True

    def is_end(self):
        self.check_set[0] = self.check_line(0, 1, 2)
        self.check_set[1] = self.check_line(3, 4, 5)
        self.check_set[2] = self.check_line(6, 7, 8)
        self.check_set[3] = self.check_line(0, 3, 6)
        self.check_set[4] = self.check_line(1, 4, 7)
        self.check_set[5] = self.check_line(2, 5, 8)
        self.check_set[6] = self.check_line(0, 4, 8)
        self.check_set[7] = self.check_line(2, 4, 6)
        for i in range(0, 7):
            if self.check_set[i] != ' ':
                self.winner = self.check_set[i]
                return True
            if self.filled == MAX_POSITIONS:
                self.winner = 'tie'
                return True
        return False

    def check_line(self, cell1, cell2, cell3):
        if self.mapgrid[cell1] == self.mapgrid[cell2] == self.mapgrid[cell3]:
            return self.mapgrid[cell1]
        else:
            return ' '

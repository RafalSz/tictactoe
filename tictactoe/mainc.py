from client import Client
from clientinterface import ClientInterface
from messages import MessageType

__author__ = 'ian'

if __name__ == "__main__":
    host = 'localhost'
    port = 50001
    data_size = 1024
    client = Client(host, port, data_size)
    clientinterface = ClientInterface(client)
    clientinterface.listenForStart()
    clientinterface.startGame()
    client.close_socket()
